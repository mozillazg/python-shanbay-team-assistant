#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

__title__ = 'shanbay-assistant'
__version__ = '0.2.7'
__author__ = 'mozillazg'
__email__ = 'mozillazg101@gmail.com'
__license__ = 'MIT'
__copyright__ = 'Copyright (c) 2014 mozillazg'
